Source: golang-github-nicksnyder-go-i18n
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-any,
               golang-github-pelletier-go-toml-dev,
               golang-gopkg-yaml.v2-dev
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-nicksnyder-go-i18n
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-nicksnyder-go-i18n.git
Homepage: https://github.com/nicksnyder/go-i18n
XS-Go-Import-Path: github.com/nicksnyder/go-i18n

Package: golang-github-nicksnyder-go-i18n-dev
Architecture: all
Depends: golang-github-pelletier-go-toml-dev,
         golang-gopkg-yaml.v2-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: goi18n
Description: Go package for i18n with templates and CLDR plural support
 go-i18n is a Go package and a command that helps you translate Go programs
 into multiple languages.
 .
  * Supports pluralized strings for all 200+ languages in the Unicode Common
    Locale Data Repository (CLDR).
  * Code and tests are automatically generated from CLDR data
  * Supports strings with named variables using text/template syntax.
  * Translation files are simple JSON or YAML.
  * Documented and tested!
 .
 This package contains the Go library github.com/nicksnyder/go-i18n/i18n
 which provides runtime APIs for fetching translated strings.

Package: goi18n
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Suggests: golang-github-nicksnyder-go-i18n-dev
Provides: go-i18n
Built-Using: ${misc:Built-Using}
Description: Formats and merges translation files for Go
 go-i18n is a Go package and a command that helps you translate Go programs
 into multiple languages.
 .
  * Supports pluralized strings for all 200+ languages in the Unicode Common
    Locale Data Repository (CLDR).
  * Code and tests are automatically generated from CLDR data
  * Supports strings with named variables using text/template syntax.
  * Translation files are simple JSON or YAML.
 .
 This package contains the /usr/bin/goi18n command which provides
 functionality for managing the translation process.
